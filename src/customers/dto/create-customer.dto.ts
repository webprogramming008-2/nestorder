import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;
  @IsNotEmpty()
  @IsPositive()
  age: number;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  gender: string;
}
